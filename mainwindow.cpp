#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    map_painter = ui->mapPainter;
    save_btn = ui->save_Btn;
    imp_line_btn = ui->impLine_btn;
    horz_scale_line = ui->horz_lineEdit;
    vert_scale_line = ui->vert_lineEdit;
    filter_enb_box = ui->filterenb_checkBx;
    filter_enble = false;
    filter_visable_box = ui->filter_checkBx;
    filter_visable = false;
    config_btn = ui->config_robot_btn;
    output_comma_btn = ui->output_comd_btn;
    imp_img_btn = ui->imp_img_btn;
    connect(save_btn, SIGNAL(clicked(bool)),this,SLOT(save_btn_pressed()));
    connect(imp_line_btn, SIGNAL(clicked(bool)),this,SLOT(imp_btn_pressed()));
    connect(horz_scale_line,SIGNAL(editingFinished()),this,SLOT(scale_changed()));
    connect(vert_scale_line,SIGNAL(editingFinished()),this,SLOT(scale_changed()));
    connect(filter_enb_box,SIGNAL(clicked(bool)),this,SLOT(filter_enb_changed()));
    connect(filter_visable_box,SIGNAL(clicked(bool)),this,SLOT(filter_visable_changed()));
    connect(config_btn,SIGNAL(pressed()),this,SLOT(config_btn_pressed()));
    connect(output_comma_btn,SIGNAL(pressed()),this,SLOT(output_command_pressed()));
    connect(imp_img_btn,SIGNAL(pressed()),this,SLOT(imp_img_pressed()));

}

void MainWindow::imp_img_pressed()
{
    QString temp_str= QFileDialog::getOpenFileName(this,QObject::tr(""),"/home");
    map_painter->setBackgroundImg(temp_str);
    QPixmap backImg = map_painter->getBackgroundImg();
    map_painter->resize(backImg.size());
    std::cout<<"back img size: "+std::to_string(backImg.height())<<std::endl;
}

void MainWindow::output_command_pressed()
{
    line_command = Line_Command();
    QString temp_str= QFileDialog::getSaveFileName(this,QObject::tr("Export Commands"),"/home");
    line_command.set_path(map_painter->get_series_line());
    line_command.output_commands(temp_str);

}

void MainWindow::keyPressEvent(QKeyEvent * ev)
{
map_painter->keyPressed2(ev->key());
}

void MainWindow::config_btn_pressed()
{
    auto_window = new Auto_Window(this);
    auto_window->exec();
}
void MainWindow::filter_visable_changed()
{
    filter_visable =filter_visable_box->isChecked();
    map_painter->showFilteredLines(filter_visable);
}

void MainWindow::filter_enb_changed()
{
   filter_enble = filter_enb_box->isChecked();
   map_painter->filterPoints(filter_enble);
}

void MainWindow::imp_btn_pressed()
{
    QString temp_str = QFileDialog::getOpenFileName(this,tr("Import Lines"),"/home");
    //map_painter->importLines(temp_str);
}

void MainWindow::save_btn_pressed()
{
    QString temp_str= QFileDialog::getSaveFileName(this,tr("Export Lines"),"/home");
    map_painter->exportLines(temp_str);
}

void MainWindow::scale_changed()
{
    std::cout<<"scale changed finished"<<std::endl;
    vert_scaleStr= vert_scale_line->text();
    horz_scaleStr = horz_scale_line->text();
    map_painter->setScaleFactor(horz_scaleStr.toFloat(), vert_scaleStr.toFloat());
}

MainWindow::~MainWindow()
{
    delete ui;
}
