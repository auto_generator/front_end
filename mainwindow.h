#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <map_painter.h>
#include <QPushButton>
#include <QLineEdit>
#include <QSlider>
#include <QLabel>
#include <QCheckBox>
#include <auto_window.h>
#include <AutoGenerator/line_command.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void save_btn_pressed();
    void imp_btn_pressed();
    void scale_changed();
    void filter_enb_changed();
    void filter_visable_changed();
    void config_btn_pressed();
    void output_command_pressed();
    void imp_img_pressed();
protected:
    void keyPressEvent(QKeyEvent * ev) override;


private:
    Ui::MainWindow *ui;
    Map_Painter *map_painter;
    QPushButton *save_btn;
    QPushButton *imp_line_btn;
    QPushButton *config_btn;
    QPushButton *output_comma_btn;
    QPushButton *imp_img_btn;
    QLineEdit *horz_scale_line;
    QLineEdit *vert_scale_line;
    QString vert_scaleStr;
    QString horz_scaleStr;
    bool filter_enble;
    QCheckBox *filter_enb_box;
    QCheckBox *filter_visable_box;
    bool filter_visable;
    Auto_Window *auto_window;
    Line_Command line_command;


};

#endif // MAINWINDOW_H
