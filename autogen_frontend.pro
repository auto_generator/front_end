#-------------------------------------------------
#
# Project created by QtCreator 2018-06-11T00:06:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = autogen_frontend
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    map_painter.cpp \
    AutoGenerator/command.cpp \
    AutoGenerator/generateauto.cpp \
    AutoGenerator/motor2.cpp \
    AutoGenerator/motorconfigw.cpp \
    auto_window.cpp \
    AutoGenerator/line_command.cpp \
    AutoGenerator/linextra.cpp

HEADERS += \
        mainwindow.h \
    map_painter.h \
    AutoGenerator/command.h \
    AutoGenerator/generateauto.h \
    AutoGenerator/motor2.h \
    AutoGenerator/motorconfigw.h \
    auto_window.h \
    AutoGenerator/line_command.h \
    AutoGenerator/linextra.h

FORMS += \
        mainwindow.ui \
    AutoGenerator/motorconfigw.ui \
    auto_window.ui
