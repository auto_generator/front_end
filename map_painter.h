#ifndef MAP_PAINTER_H
#define MAP_PAINTER_H

#include <QWidget>
#include <QPen>
#include <QPainter>
#include <QMouseEvent>
#include <vector>
#include <auto_window.h>
#include <AutoGenerator/linextra.h>

class Map_Painter : public QWidget
{
    Q_OBJECT

public:
    Map_Painter(QWidget *parent = 0);
    Map_Painter();
    void exportLines(QString path);
    void setScaleFactor(float xScale_arg,float yScale_arg);
    void importLines(QString path);
    void filterPoints(bool yes);
    void showFilteredLines(bool showFilterArg);
        void keyPressed2(int keyVal);
    std::vector<QLineF> get_series_line();
    void setBackgroundImg(QString backImg_arg);
    QPixmap getBackgroundImg();


protected:
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;
    void mousePressEvent(QMouseEvent * ev) override;
    void mouseMoveEvent(QMouseEvent * ev) override;
    void draw(const QPoint & pos);
    void drawFilteredLines();


private:
    QPixmap m_pixmap;
    QPoint m_lastPos;
    std::vector<QPoint> series_points;
    std::vector<QLine> series_lines;
    std::vector<QLine> filtered_series_lines;
    std::vector<QLineF> filtered_series_lines_scaled;
    std::vector<Linextra> series_lines_slopes;
    void reset();
    QPixmap background = QPixmap("/home/rommac100/Desktop/image009.jpg");
    float xScale;
    float yScale;
    bool filter_points;
    bool showFilter;
    void refreshFilteredLines();
    void refreshScaledLines();

};
#endif // MAP_PAINTER_H

