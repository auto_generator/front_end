#include "map_painter.h"
#include <iostream>
#include <fstream>
#include <math.h>
#include <algorithm>
#include <time.h>



Map_Painter::Map_Painter()
{
m_pixmap = background;
setScaleFactor(1,1);
}

Map_Painter::Map_Painter(QWidget *parent) : QWidget(parent)
{
m_pixmap = background;
setScaleFactor(1,1);
}

void Map_Painter::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawPixmap(0,0, m_pixmap);
}

QPixmap Map_Painter::getBackgroundImg()
{
    return background;
}

void Map_Painter::setBackgroundImg(QString backImg_arg)
{
    std::cout<<"backImg dir: "+backImg_arg.toStdString()<<std::endl;
    if (backImg_arg != "")
    {
    background = QPixmap(backImg_arg);
    }
    reset();
}

static std::vector<QLineF> convertPoint_PointF(std::vector<QLine> temp)
{
    std::vector<QLineF> temp_series;
    for (QLine i :temp)
    {
        temp_series.push_back(QLineF(i));
    }
    return temp_series;
}


std::vector<QLineF> Map_Painter::get_series_line()
{
    if (xScale != 1 || yScale !=1)
    {

        return filtered_series_lines_scaled;
    }
    else if (filter_points)
    {
        return convertPoint_PointF(filtered_series_lines);
    }
    else
    {
        return convertPoint_PointF(series_lines);
    }

}

void Map_Painter::setScaleFactor(float xScale_arg, float yScale_arg)
{
    xScale = xScale_arg;
    yScale = yScale_arg;
     std::cout<<"scale X Val: "+std::to_string(xScale)<<std::endl;
    refreshScaledLines();
}

bool checkForDuplicates(std::vector<int> temp_vect, int number)
{
    for (int temp : temp_vect)
    {
        if (temp == number)
        {
            return false;
        }
    }
    return true;
}

//filters about half of the interior points on the given path then reconnects them. Essentially decreasing the resolution of the path
void Map_Painter::refreshFilteredLines()
{
    if (series_points.size() > 0)
    {
    QPoint firstPoint = series_points.at(0);
    QPoint lastPoint = series_points.at(series_points.size()-1);
    std::vector<int> filteredIndexs;
    std::vector<QPoint> tempFilteredPoints;
    std::vector<QLine> tempFilteredLines;
    tempFilteredPoints.push_back(firstPoint);
    for (int i =1; i < (series_points.size()-3); i+=2)
    {
        tempFilteredPoints.push_back(series_points.at(i));
    }
    tempFilteredPoints.push_back(lastPoint);

    for (int i =0; i< tempFilteredPoints.size()-1; i++)
    {
      tempFilteredLines.push_back(QLine(tempFilteredPoints.at(i),tempFilteredPoints.at(i+1)));
    }
    filtered_series_lines = tempFilteredLines;
    }
}

void Map_Painter::drawFilteredLines()
{
    if (filtered_series_lines.size() >0 && showFilter)
    {
        refreshFilteredLines();
        QPainter painter{&m_pixmap};
        painter.setRenderHint(QPainter::Antialiasing);
        painter.setPen({Qt::red, 2.0});
        for (int i=0; i< filtered_series_lines.size(); i++)
        {
            painter.drawLine(filtered_series_lines.at(i));
        }
        update();
    }
        else if (!showFilter && series_lines.size() > 0){
            reset();
            QPainter painter{&m_pixmap};
            painter.setRenderHint(QPainter::Antialiasing);
            painter.setPen({Qt::blue, 2.0});
            for (int i=0; i< series_lines.size(); i++)
            {

                painter.drawLine(series_lines.at(i));
            }
            update();
        }
}

void Map_Painter::filterPoints(bool yes)
{
    filter_points = yes;
    if (yes)
    {
        refreshFilteredLines();

    }
     drawFilteredLines();
}

void Map_Painter::mousePressEvent(QMouseEvent *ev)
{
    m_lastPos = ev->pos();
    series_points.push_back(m_lastPos);
    if (series_points.size() > 1)
    {
     series_lines.push_back(QLine(series_points.at(series_points.size()-1),series_points.at(series_points.size()-2)));
     std::cout<<series_lines.size()<<std::endl;
    }
            draw(ev->pos());
            //setFocus();
}

void Map_Painter::mouseMoveEvent(QMouseEvent *ev)
{
draw(ev->pos());
series_points.push_back(m_lastPos);
if (series_points.size() > 1)
{
 series_lines.push_back(QLine(series_points.at(series_points.size()-1),series_points.at(series_points.size()-2)));
}
}

void Map_Painter::keyPressed2(int keyVal)
{
    if (keyVal == 88)
    {
        m_pixmap = background;
        series_lines = std::vector<QLine>();
        series_points = std::vector<QPoint>();
        reset();
    }
}

void Map_Painter::refreshScaledLines()
{
    if (xScale != 1 || yScale != 1)
    {
        std::vector<QLine> tempLines;
        std::vector<QLineF> temp_lines_scaled;

    if (filter_points)
    {
        tempLines = filtered_series_lines;
    }
    else
    {
        tempLines = series_lines;
    }
        for (int i =0; i < tempLines.size(); i++)
        {
            QLine temp = tempLines.at(i);
            QPoint firstPoint = temp.p1();
            QPoint secondPoint = temp.p2();
            QPointF firstPointF = QPointF(firstPoint);
            firstPointF.setX(firstPoint.x()* pow(xScale,-1));
            firstPointF.setY(firstPoint.y()* pow(yScale,-1));
            QPointF secondPointF = QPointF(secondPoint);
            secondPointF.setX(secondPoint.x()*pow(xScale,-1));
            secondPointF.setY(secondPoint.y()*pow(yScale,-1));

            temp_lines_scaled.push_back(QLineF(firstPointF,secondPointF));
        }

        filtered_series_lines_scaled = temp_lines_scaled;
    }
}

void Map_Painter::showFilteredLines(bool showFilterArg)
{
    showFilter = showFilterArg;
    if (showFilter)
    {
        drawFilteredLines();
    }
}

void Map_Painter::exportLines(QString path)
{
    std::ofstream output;

    output.open(path.toStdString());
    if (xScale == 1 || yScale == 1)
    {
        std::vector<QLine> temp_series;
    if (filter_points)
    {
        temp_series = filtered_series_lines;
    }
    else
    {  temp_series = series_lines;}
    if (temp_series.size() >0)
    {
        for (QLine tempLine : temp_series)
        {
            std::string temp = std::to_string(tempLine.x1())+","+std::to_string(tempLine.y1())+","+std::to_string(tempLine.x2())+","+std::to_string(tempLine.y2())+"\n";
            output << temp;
        }
    }
    }
    else
    {
        std::cout<<"EXPORTING SCALED"<<std::endl;
        std::vector<QLineF> temp_series;
        for (QLineF tempLine: temp_series)
        {
            std::string temp = std::to_string(tempLine.x1())+","+std::to_string(tempLine.y1())+","+std::to_string(tempLine.x2())+","+std::to_string(tempLine.y2())+"\n";
            output << temp;
        }

    }
    output.close();
}

std::vector<std::string> parseCSV(std::string temp)
{
    std::vector<std::string> output;

    std::string temp2 = temp;
std::cout<<"line: "+temp2<<std::endl;
    while (temp2.find_first_of(",") != std::string::npos)
    {
        int index = temp2.find_first_of(",");

        //std::cout<<"index of comma: "+std::to_string(index)<<std::endl;
        output.push_back(temp2.substr(0,index));
        temp2 = temp2.substr(index,(temp2.length()));
        //std::cout<<"parsing csv"<<std::endl;
    }

}

//Currently Not working parseCSV method gets stuck in infinite loop
void Map_Painter::importLines(QString path)
{
    std::ifstream import(path.toStdString());
    std::string temp;
    if (import.is_open())
    {
        while(getline(import,temp))
        {
            std::cout<<"line: "+temp<<std::endl;
            std::vector<std::string> tempList = parseCSV(temp);
            if (tempList.size() ==4)
            {
                QLine tempQLine = QLine(std::stoi(tempList.at(0)),std::stoi(tempList.at(1)),std::stoi(tempList.at(2)),std::stoi(tempList.at(3)));
                series_lines.push_back(tempQLine);
        }
            }
        std::cout<<"passed parsing"<<std::endl;
    import.close();
    QPainter painter{&m_pixmap};
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen({Qt::blue, 2.0});
    for (QLine i : series_lines)
    {
        painter.drawLine(i);
    }
    update();
}
}

void Map_Painter::reset()
{
    QPainter painter{&m_pixmap};
    painter.setRenderHints(QPainter::Antialiasing);
    painter.drawPixmap(0,0,background);
    series_points = std::vector<QPoint>();
    series_lines = std::vector<QLine>();
    filtered_series_lines = std::vector<QLine>();
    filtered_series_lines_scaled = std::vector<QLineF>();
    series_lines_slopes = std::vector<Linextra>();
    update();
}

void Map_Painter::resizeEvent(QResizeEvent *event)
{
    auto newRect = m_pixmap.rect().united(rect());
            if (newRect == m_pixmap.rect()) return;
            QPixmap newPixmap{newRect.size()};
            QPainter painter{&newPixmap};
            painter.fillRect(newPixmap.rect(), Qt::white);
            painter.drawPixmap(0, 0, m_pixmap);
            m_pixmap = newPixmap;
}

void Map_Painter::draw(const QPoint &pos)
{
    QPainter painter{&m_pixmap};
            painter.setRenderHint(QPainter::Antialiasing);
            painter.setPen({Qt::blue, 2.0});
            painter.drawLine(m_lastPos, pos);
            m_lastPos = pos;
            update();
}


