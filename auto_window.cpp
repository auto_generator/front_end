#include <QFileDialog>
#include <QDir>
#include <QEventLoop>
#include "auto_window.h"
#include "ui_auto_window.h"

Auto_Window::Auto_Window(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Auto_Window)
{
    ui->setupUi(this);
    deviceCombo = ui->deviceCombo;
        deviceList = ui->deviceList;
        initializeDeviceCombo();
        generateAuto = GenerateAuto();
}

Auto_Window::~Auto_Window()
{
    delete ui;
}

void Auto_Window::initializeDeviceCombo()
{
    deviceCombo->addItem("Motor");
    deviceCombo->addItem("Servo");
    deviceCombo->addItem("Sensor");
}

void Auto_Window::on_addDevBut_clicked()
{
    QString deviceInfo = "temp";
    QEventLoop eventLoop;
    Motor2 motor2;
    switch (currentDeviceType)
    {
    case Servo:
        generateAuto.add_to_servos(currentDeviceSelect.toStdString());
        break;
    case MotorE:
        motorConfigWin = new MotorConfigW(this);
        motorConfigWin->exec();
        //connect(this, SIGNAL(destroyed()), & eventLoop, SLOT(on_MotorConfigDestroyed()));
        //Motor2 motor2;
        motor2 = Motor2(motorConfigWin->getMotorName().toStdString(),motorConfigWin->getInversion(),motorConfigWin->getDriveMotor());
        motor2.setSideDir(motorConfigWin->getSideDir());
        generateAuto.add_to_motors(motor2);

        deviceInfo = motorConfigWin->getMotorName()+"Motor-"+"Drive-"+QString::number(motorConfigWin->getDriveMotor())+"-Invert-"+QString::number(motorConfigWin->getInversion())+"-SideDir-"+QString::fromStdString(Motor2::parseSideDir(motor2.getSideDir()));
        break;
    case Sensor:
        generateAuto.add_to_sensors(currentDeviceSelect.toStdString());
        break;
    default:
        std::cout<<"ERROR:::: DONT KNOW WHAT TYPE THIS IS."<<std::endl;
        break;
    }
    deviceList->addItem(deviceInfo);
}
void Auto_Window::on_deviceCombo_activated(int index)
{
    currentDeviceSelect = deviceCombo->currentText();
    switch (index)
    {
    case 0:
        currentDeviceType = MotorE;
        break;
    case 1:
        currentDeviceType = Servo;
        break;
    case 2:
        currentDeviceType = Sensor;
        break;
    default:
        currentDeviceType = MotorE;
        break;
    }
}

int Auto_Window::exec()
{
    std::vector<Motor2> list_motors;
    std::cout << "list_motors_size: "+ to_string(list_motors.size())<<std::endl;
    QString deviceInfo = "temp";
    list_motors = generateAuto.get_list_motors();
    if (list_motors.size() > 0)
    {
        std::cout << "list_motors_size: "+ to_string(list_motors.size())<<std::endl;
        QString deviceInfo = "temp";
        for (int i =0; i < list_motors.size(); i++)
        {
            Motor2 motor2 = list_motors.at(i);

            deviceInfo = QString::fromStdString(motor2.getMotorName())+"Motor-"+"Drive-"+QString::number(motor2.isDriveMotor())+"-Invert-"+QString::number(motor2.getInversion())+"-SideDir-"+QString::fromStdString(Motor2::parseSideDir(motor2.getSideDir()));
            deviceList->addItem(deviceInfo);
        }
    }
    QDialog::exec();

    return 1;
}

void Auto_Window::on_MotorConfigDestroyed()
{

}


void Auto_Window::on_outputCBtn_clicked()
{
    std::cout<<"testing export"<<endl;
     QString temp_str= QFileDialog::getSaveFileName(this,tr("Export Setup Code"),"/home");
     std::string temp = temp_str.toStdString();
   generateAuto.outputCode(temp);
}

void Auto_Window::on_imp_btn_clicked()
{
    QString temp_str = QFileDialog::getOpenFileName(this,tr("Import Config"),"/home");
    std::cout<<"testing Import"<<std::endl;
    generateAuto.importConfigPath(temp_str.toStdString());
    std::vector<Motor2> list_motors;
    list_motors = generateAuto.get_list_motors();
    if (list_motors.size() > 0)
    {
        for (int i =0; i < list_motors.size(); i++)
        {
            Motor2 motor2 = list_motors.at(i);
            QString deviceInfo = QString::fromStdString(motor2.getMotorName())+"Motor-"+"Drive-"+QString::number(motor2.isDriveMotor())+"-Invert-"+QString::number(motor2.getInversion())+"-SideDir-"+QString::fromStdString(Motor2::parseSideDir(motor2.getSideDir()));
            deviceList->addItem(deviceInfo);
        }
    }

}

void Auto_Window::on_exp_config_btn_clicked()
{
    std::cout<<"testing export"<<endl;
     QString temp_str= QFileDialog::getSaveFileName(this,tr("Export Setup Code"),"/home");
     std::string temp = temp_str.toStdString();
     generateAuto.exportConfig(temp);
}
