#include "generateauto.h"
#include <fstream>
#include <sstream>

using namespace std;

static std::vector<std::string> split(std::string &s, char delim)
{
    std::stringstream ss(s);
    std::string item;
    vector<std::string> tokens;
    while (getline(ss,item,delim))
    {
        tokens.push_back(item);
    }
    return tokens;
}

static std::vector<std::string> parseComma(std::string &unParsed)
{
    return split(unParsed,',');
}


const char * const BoolToString(bool b)
{
  return b ? "true" : "false";
}

GenerateAuto::GenerateAuto(string path_arg)
{
 path = path_arg;
}
GenerateAuto::GenerateAuto()
{

}

GenerateAuto::GenerateAuto(vector<Motor2> motors_arg, vector<string> servos_arg, vector<string> sensors_arg)
{
    motors = motors_arg;
    servos = servos_arg;
    sensors = sensors_arg;
}

void GenerateAuto::add_to_motors(Motor2 motor_arg)
{
    motors.push_back(motor_arg);
    refresh_drive_motors();
}

void GenerateAuto::refresh_drive_motors()
{
    vector<Motor2> temp;
    for (Motor2 i: motors)
    {
        if (i.isDriveMotor())
            temp.push_back(i);
    }
    drive_motors = temp;
}

std::vector<Motor2> GenerateAuto::get_list_motors()
{
    return motors;
}

void GenerateAuto::add_to_servos(string servo_arg)
{
    servos.push_back(servo_arg);

}
void GenerateAuto::add_to_sensors(string sensor_arg)
{
    sensors.push_back(sensor_arg);

}
void GenerateAuto::adjust_motor_inversion(string motorName, bool inversion)
{

}

//format - (Device Type,Name,Inversion,Secondary Inversion)

void GenerateAuto::exportConfig(std::string path_arg)
{
    std::cout<<"Exporting Config"<<std::endl;
    ofstream file_output(path_arg);
    vector<string> output;
    string temp;
      std::cout<<"Exporting Config - Mot"<<std::endl;
    for (Motor2 tempM: motors)
    {
      temp = "M,"+tempM.getMotorName()+","+BoolToString(tempM.getInversion())+","+BoolToString(tempM.isDriveMotor())+","+Motor2::parseSideDir(tempM.getSideDir())+"\n";
        output.push_back(temp);
    }

    for (int i = 0; i < output.size(); i++)
    {
        file_output<<output.at(i);
    }
    file_output.close();
}

bool StringToBoolean(string temp)
{
    bool tempBol = false;
    if (temp == "true")
    {tempBol = true;}
      return tempBol;
}


void GenerateAuto::importConfigPath(string path_arg)
{
    ifstream myFile(path_arg);
    if(myFile.is_open())
    {
        std::cout<<"successfully opened desired config file"<<std::endl;
    std::vector<string> series_string;
    string temp;
    while (getline(myFile,temp))
    {
        series_string.push_back(temp);
    }
    importConfig(series_string);
    myFile.close();
    }
}

void GenerateAuto::importConfig(vector<string> configArg)
{

    for (int i =0; i < configArg.size(); i++)
    {
        std::vector<string> temp = parseComma(configArg.at(i));
        if (temp[0] == "M")
        {
            bool inversion1 = StringToBoolean(temp[2]);
            bool inversion2 = StringToBoolean(temp[3]);
            SideDir temp2 = Motor2::parseString(temp[4]);
            std::cout<<"SIDE DIR IMPORTED - "+Motor2::parseSideDir(temp2)<<std::endl;
            Motor2 temp3 =  Motor2(temp[1],inversion1,inversion2);
            temp3.setSideDir(temp2);
            add_to_motors(temp3);
        }
        else if (temp[0] == "S")
        {

        }
        else if (temp[0] == "Sn")
        {

        }
    }
}
vector<string> GenerateAuto::set_drive_motors_dir(bool inversion)
{
    vector<string> output;
    string temp;
    for (Motor2 i : drive_motors)
    {
        temp = "robot."+i.getMotorName()+".setDirection(DcMotor.Direction.";
        if (!inversion)
        {
            temp += "FORWARD);\n";
        }
        else
        {
            temp+= "REVERSE);\n";
        }
        output.push_back(temp);
        }
    return output;
    }
vector<string> GenerateAuto::set_drive_motors_runmode(RunMode runMode)
{
    string runModeS;
    vector<string> output;
    string tempLine;
    switch (runMode)
    {
        case RUN_TO_POSITION:
        runModeS = "RUN_TO_POSITION";
        break;
    case STOP_AND_RESET_ENCODER:
        runModeS = "STOP_AND_RESET_ENCODER";
        break;
    case RUN_USING_ENCODER:
        runModeS = "RUN_USING_ENCODER";
        break;
    case RUN_WITHOUT_ENCODER:
        runModeS = "RUN_WITHOUT_ENCODER";
        break;
    default:
       runModeS = "RUN_WITHOUT_ENCODER";
       break;
    }

    for (Motor2 i: drive_motors)
    {
            tempLine = "robot."+i.getMotorName()+".setMode(DcMotor.RunMode."+runModeS+");\n";
            output.push_back(tempLine);

    }
    return output;

}

vector<string> GenerateAuto::set_drive_motors_brake_settings(BrakeSettings brakeSettings)
{
    string brakeSettingsS;
    vector<string> output;
    string tempLine;
    switch (brakeSettings)
    {
        case FLOAT:
        brakeSettingsS = "FLOAT";
        break;
    case BRAKE:
        brakeSettingsS = "BRAKE";
        break;
    default:
       brakeSettingsS = "FLOAT";
       break;
    }

    for (Motor2 i: drive_motors)
    {

            tempLine = "robot."+i.getMotorName()+".setZeroPowerBehavior(DcMotor.ZeroPowerBehavior."+brakeSettingsS+");\n";
            output.push_back(tempLine);

    }
    return output;

}

vector<string> GenerateAuto::set_drive_motors_pwr(std::string leftPwr, std::string rightPwr)
{
    vector<string> tempOutput;
    std::string tempS;
    for (Motor2 temp: motors)
    {
       tempS = "robot."+temp.getMotorName()+".setPower(";
       if (temp.getSideDir() == LEFT)
       {
           tempS+= leftPwr+");\n";
       }
       else if (temp.getSideDir() == RIGHT)
       {
           tempS+= rightPwr+");\n";
       }
       tempOutput.push_back(tempS);
    }
    return tempOutput;
}

void GenerateAuto::outputCode(string path_arg)
{
    path = path_arg;
    std::cout<<"IN ExPORT FUNCTION"<<std::endl;
    ofstream file_output(path_arg);

    if (file_output.is_open())
    {
    vector<string> output;
    string temp = "public enum Type {FORWARD,REVERSE,CLOCKWISE, COUNTCLOCK,IDLE };\n";
    output.push_back(temp);
    temp = "public double adjustAngle(double angle){while(angle>180) {angle-=360;}while(angle<= -180){angle+=360;}return angle;}\n";
    output.push_back(temp);
    temp = "public void setup(){\n";
    output.push_back(temp);
    std::cout<<"test before for";
    for (Motor2 i : motors)
    {
        std::cout <<"made it here pop";
        temp = "robot."+i.getMotorName()+".setDirection(DcMotor.Direction.";
        if (!i.getInversion())
        {
            temp += "FORWARD);\n";
        }
        else
        {
            temp+= "REVERSE);\n";
        }
        output.push_back(temp);
    }
    temp = "}\n";
    output.push_back(temp);
    temp = "public void doV(Type type, double magnitude){\n";
    output.push_back(temp);
    temp = "switch(type){\n";
    output.push_back(temp);
    //begininng of forward case
    temp = "case FORWARD:\n";
    output.push_back(temp);
    vector<string> temp2 = set_drive_motors_dir(false);
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp2 = set_drive_motors_runmode(RUN_TO_POSITION);
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp2 = set_drive_motors_brake_settings(BRAKE);
    output.insert(output.end(),temp2.begin(),temp2.end());

    for (Motor2 i : drive_motors)
    {
            temp = "robot."+i.getMotorName()+".setTargetPosition((int)(magnitude)+robot."+i.getMotorName()+".getCurrentPosition());\n";
            output.push_back(temp);
    }
    temp = "double timer = System.nanoTime()+(8*Math.pow(10,9));\n";
    output.push_back(temp);
    temp2 = set_drive_motors_pwr(to_string(motPow),to_string(motPow));
    output.insert(output.end(),temp2.begin(),temp2.end());

    temp = "while (robot."+drive_motors.at(0).getMotorName()+".isBusy() && opModeIsActive() && timer > System.nanoTime()){\n";
    output.push_back(temp);

    temp = "telemetry.addData(\"Distance Traveled \", robot."+drive_motors.at(0).getMotorName()+".getCurrentPosition());\n";
    output.push_back(temp);
    temp = "telemetry.update();\n";
    output.push_back(temp);
    temp = "idle();";
    output.push_back(temp);
    for (Motor2 i: drive_motors)
    {
            temp = "robot."+i.getMotorName()+".setPower("+to_string(0)+");\n";
            output.push_back(temp);
    }
    temp= "}\n";
    output.push_back(temp);
    temp2 = set_drive_motors_runmode(STOP_AND_RESET_ENCODER);
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp2 = set_drive_motors_runmode(RUN_WITHOUT_ENCODER);
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp = "break;\n";
    //end of Forward Case
    output.push_back(temp);
    //beginning of Reverse Case
    temp = "case REVERSE:\n";
    output.push_back(temp);
    temp2 = set_drive_motors_dir(false);
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp2 = set_drive_motors_runmode(RUN_TO_POSITION);
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp2 = set_drive_motors_brake_settings(BRAKE);
    output.insert(output.end(),temp2.begin(),temp2.end());
    for (Motor2 i : motors)
    {
            temp = "robot."+i.getMotorName()+".setTargetPosition((int)(-magnitude)+robot."+i.getMotorName()+".getCurrentPosition());\n";
            output.push_back(temp);
    }
    temp = "timer = System.nanoTime()+(8*Math.pow(10,9));\n";
    output.push_back(temp);
    temp2 = set_drive_motors_pwr(to_string(motPow),to_string(motPow));
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp = "while (robot."+drive_motors.at(0).getMotorName()+".isBusy() && opModeIsActive() && timer > System.nanoTime()){\n";
    output.push_back(temp);

    temp = "telemetry.addData(\"Distance Traveled \", robot."+drive_motors.at(0).getMotorName()+".getCurrentPosition());\n";
    output.push_back(temp);
    temp = "telemetry.update();\n";
    output.push_back(temp);
    temp = "idle();";
    output.push_back(temp);
    temp2 = set_drive_motors_pwr("0","0");
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp= "}\n";
    output.push_back(temp);
    temp2 = set_drive_motors_runmode(STOP_AND_RESET_ENCODER);
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp2 = set_drive_motors_runmode(RUN_WITHOUT_ENCODER);
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp = "break;\n";
    output.push_back(temp);
    //end of reverse case
    //start turn Clockwise case

    temp = "case CLOCKWISE:\n";
    output.push_back(temp);
    temp2 = set_drive_motors_brake_settings(BrakeSettings::BRAKE);
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp ="double targetAngle = adjustAngle((getHeading() + magnitude));\n";
    output.push_back(temp);
    temp = "double error;\n";
    output.push_back(temp);
    temp = "double prevError = 0;\n";
    output.push_back(temp);
    temp = "double power;\n";
    output.push_back(temp);
    temp = "double kp = "+ to_string(turn_p)+";\n";
    output.push_back(temp);
    temp = "double kd = "+ to_string(turn_d)+";\n";

    output.push_back(temp);
    temp = "double currentTime = 0;\n";
    output.push_back(temp);
    temp = " double previousTime =0;\n";
    output.push_back(temp);
    temp = "double deriv;\n\n";
    output.push_back(temp);
    temp = "ElapsedTime runTime = new ElapsedTime();\n";
    output.push_back(temp);
    temp = "runTime.reset();\n";
    output.push_back(temp);
    temp = "currentTime = runTime.seconds();\n";
    output.push_back(temp);
    temp= "previousTime = currentTime;\n";
    output.push_back(temp);
    temp = "double timeOut = currentTime+6;\n";
    output.push_back(temp);
    temp = "do {\n";
    output.push_back(temp);
    temp = "currentTime = runTime.seconds();\n";
    output.push_back(temp);
    temp = "error = adjustAngle(targetAngle-getHeading());\n";
    output.push_back(temp);
    temp = "deriv = (error-prevError)/(currentTime-previousTime);\n";
    output.push_back(temp);
    temp = "power = kp*error+(kd*deriv);\n";
    output.push_back(temp);
    temp2 = set_drive_motors_pwr("-power", "power");
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp = "idle();\n";
    output.push_back(temp);
    temp = "previousTime = currentTime;\n";
    output.push_back(temp);
    temp = "} while (opModeIsActive() && Math.abs(error) > 3 && timeOut > runTime.seconds());\n";
    output.push_back(temp);
    temp2 = set_drive_motors_pwr("0","0");
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp = "break;\n";
    output.push_back(temp);
    //End of Clockwise Turn
    //start of Count CLOCK
    temp = "case COUNTCLOCK:\n";
    output.push_back(temp);
    temp2 = set_drive_motors_brake_settings(BrakeSettings::BRAKE);
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp ="targetAngle = adjustAngle((getHeading() + magnitude));\n";
    output.push_back(temp);
    temp = "prevError = 0;\n";
    output.push_back(temp);
    temp = "kp = "+ to_string(turn_p)+";\n";
    output.push_back(temp);
    temp = "kd = "+ to_string(turn_d)+";\n";

    output.push_back(temp);
    temp = "currentTime = 0;\n";
    output.push_back(temp);
    temp = "previousTime =0;\n";
    output.push_back(temp);
    temp = "runTime = new ElapsedTime();\n";
    output.push_back(temp);
    temp = "runTime.reset();\n";
    output.push_back(temp);
    temp = "currentTime = runTime.seconds();\n";
    output.push_back(temp);
    temp= "previousTime = currentTime;\n";
    output.push_back(temp);
    temp = "timeOut = currentTime+6;\n";
    output.push_back(temp);
    temp = "do {\n";
    output.push_back(temp);
    temp = "currentTime = runTime.seconds();\n";
    output.push_back(temp);
    temp = "error = adjustAngle(targetAngle-getHeading());\n";
    output.push_back(temp);
    temp = "deriv = (error-prevError)/(currentTime-previousTime);\n";
    output.push_back(temp);
    temp = "power = kp*error+(kd*deriv);\n";
    output.push_back(temp);
    temp2 = set_drive_motors_pwr("power", "-power");
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp = "idle();\n";
    output.push_back(temp);
    temp = "previousTime = currentTime;\n";
    output.push_back(temp);
    temp = "} while (opModeIsActive() && Math.abs(error) > 3 && timeOut > runTime.seconds());\n";
    output.push_back(temp);
    temp2 = set_drive_motors_pwr("0","0");
    output.insert(output.end(),temp2.begin(),temp2.end());
    temp = "break;\n";
    output.push_back(temp);



    //end of switch statement
    temp = "}\n";
    output.push_back(temp);
    temp = "}\n";
    output.push_back(temp);
     //close statement
    for (std::string i :output)
    {
        file_output<<i;
    }
    file_output.close();
    }

}
