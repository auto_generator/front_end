#include "motorconfigw.h"
#include "ui_motorconfigw.h"
#include <iostream>

MotorConfigW::MotorConfigW(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MotorConfigW)
{
    ui->setupUi(this);
    invertMotBol= false;
    driveMotBol = false;
    motorName = "temp";
    sideDirCombo = ui->drive_dir_comb;
    sideDirCombo->setVisible(false);
    sideDirLbl = ui->drive_dir_lbl;
    sideDirLbl->setVisible(false);
    sideDir = Null;
    motorNameEdit = ui->motorNamEdit;
}

MotorConfigW::~MotorConfigW()
{
    delete ui;
}

void MotorConfigW::on_saveSetBtn_clicked()
{
    this->close();
}

QString MotorConfigW::getMotorName()
{
    return motorName;
}

bool MotorConfigW::getInversion()
{
    return invertMotBol;
}

bool MotorConfigW::getDriveMotor()
{
    return driveMotBol;
}

void MotorConfigW::on_driveMotBol_toggled(bool checked)
{
    driveMotBol = checked;
    if (driveMotBol)
    {
        sideDirCombo->setVisible(true);
        sideDirLbl->setVisible(true);
    }
    else
    {
        sideDirCombo->setVisible(false);
        sideDirLbl->setVisible(false);
        sideDir = Null;
    }
}

void MotorConfigW::on_invertMotBol_toggled(bool checked)
{
    invertMotBol = checked;
}

void MotorConfigW::on_motorNamEdit_editingFinished()
{
    motorName = motorNameEdit->displayText();
}

void MotorConfigW::on_drive_dir_comb_currentIndexChanged(int index)
{
    std::cout<<"INDEX FOR DIR SELECTOR: "+ std::to_string(index)<<std::endl;
    if (index == 0)
    {
        sideDir = LEFT;
    }
    else if (index == 1)
    {
        sideDir = RIGHT;
    }
    else
    {
        sideDir = Null;
    }
    std::cout<<"SIDE DIR VALUE: "+ Motor2::parseSideDir(sideDir)<<std::endl;
}

SideDir MotorConfigW::getSideDir()
{
    return sideDir;
}
