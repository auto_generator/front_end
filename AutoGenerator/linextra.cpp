#include "linextra.h"

Linextra::Linextra()
{

}

Linextra::Linextra(QLine line_arg)
{
    line = QLineF(line_arg);
    slope = (double)(line.dy())/(line.dx());
}

Linextra::Linextra(QLineF line_arg)
{
    line = line_arg;
    slope = (double)(line.dy())/(line.dx());
}

double Linextra::getSlope()
{
return slope;
}

QLineF Linextra::getQLineF()
{
    return line;
}

QPointF Linextra::p1()
{
    return line.p1();
}
QPointF Linextra::p2()
{
    return line.p2();
}


