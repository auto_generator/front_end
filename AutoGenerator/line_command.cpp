#include "line_command.h"
#include "math.h"
#include <fstream>
#include <QString>
#include <QFileDialog>
#include <iostream>
#include <QObject>

    const double PI = 3.14156;

Line_Command::Line_Command()
{

}


double calcDistance(QLineF temp_line)
{
    QPointF fPoint = temp_line.p1();
    QPointF lPoint = temp_line.p2();

    double diffX = (double)(pow((double)(fPoint.x()-lPoint.x()),2));
    double diffY = pow ((fPoint.y()-lPoint.y()),2);
    double temp = pow((diffX+diffY),.5);
    return temp;
}

static double toDegrees(double radian)
{
    return radian*(180.0)/PI;
}


void Line_Command::generate_commands()
{
   std::vector<Command> tempCom;
    if (path.size() > 0)
    {
        for (int i =0; i< path.size(); i++)
        {
            QLineF temp = path.at(i);
            double hDist = calcDistance(temp);

            double angle = toDegrees(sin((temp.dx()/hDist)));
            if (angle > 0)
            {
               Command temp = Command(CLOCKWISE, angle);
               tempCom.push_back(temp);
            }
            else if (angle < 0)
            {
                Command temp = Command(COUNTCLOCK, abs(angle));
                tempCom.push_back(temp);
            }
            tempCom.push_back(Command(FORWARD,hDist));
        }
    }
    list_commands = tempCom;
}

std::vector<std::string> Line_Command::output_commands(QString tempDir)
{
    generate_commands();
    std::vector<std::string> command_output;
    std::cout<<"path-Size: "+ std::to_string(path.size())<<std::endl;
    if (path.size() > 0)
    {
        for (int i =0; i< list_commands.size(); i++)
        {

            std::cout<<"OUTPUTING COMMANDS: "+list_commands.at(i).output_command()<<std::endl;
            command_output.push_back(list_commands.at(i).output_command());
        }
    }

    std::cout<<"Exporting Commands"<<std::endl;
    ofstream file_output(tempDir.toStdString());

    for (int i =0; i < command_output.size(); i++)
    {
        file_output<<command_output.at(i)+"\n";
    }
    file_output.close();
    return command_output;
}

void Line_Command::set_path(std::vector<QLineF> arg_path)
{
    path = arg_path;
}
